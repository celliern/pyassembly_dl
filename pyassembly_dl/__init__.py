import importlib.metadata
from .core import AssemblyDownloader

__version__ = importlib.metadata.version("pyassembly_dl")