from dataclasses import dataclass
from functools import cached_property
from typing import Literal
from functools import partial
import polars as pl
import pooch
from tqdm.contrib.concurrent import thread_map
from loguru import logger

pooch_logger = pooch.get_logger()
pooch_logger.setLevel("ERROR")


@dataclass
class AssemblyDownloader:
    """Download reference genomes from NCBI using assembly accessions.

    Parameters
    ----------
    folder : str
        Folder to store downloaded files.
    db : {"refseq", "genbank"}, default="refseq"
        Database to download from.
    subdir : str, default="bacteria"
        Subdirectory to download from.
    ncbi_ftp_url : str, default="https://ftp.ncbi.nlm.nih.gov"
        NCBI FTP base url.

    Attributes
    ----------
    assembly_summary_url : str
        URL to assembly summary file.
    assembly_summary_filename : str
        Path to assembly summary file.
    assembly_summary : polars.DataFrame
        Assembly summary file as a polars.DataFrame.
    download_paths : polars.DataFrame
        Download paths for reference genomes as a polars.DataFrame.
    """

    folder: str
    db: Literal["refseq", "genbank"] = "refseq"
    subdir: str = "bacteria"
    ncbi_ftp_url: str = "https://ftp.ncbi.nlm.nih.gov"

    @property
    def assembly_summary_url(self):
        return (
            f"{self.ncbi_ftp_url}/genomes/{self.db}/{self.subdir}/assembly_summary.txt"
        )

    @property
    def assembly_summary_filename(self):
        logger.info("Downloading assembly summary file (can be a bit long)")
        return pooch.retrieve(
            url=self.assembly_summary_url,
            known_hash=None,
            progressbar=True,
        )

    @cached_property
    def assembly_summary(self):
        return pl.read_csv(
            self.assembly_summary_filename,
            separator="\t",
            skip_rows=1,
            null_values=["na"],
            infer_schema_length=None,
        )

    @property
    def download_paths(self):
        basename = pl.col("ftp_path").str.split("/").list.get(-1)
        return (
            self.assembly_summary.filter(
                (pl.col("version_status") == "latest")
                & (
                    pl.col("refseq_category").is_in(
                        ["reference genome", "representative genome"]
                    )
                )
            )
            .select(
                accession="#assembly_accession",
                ftp_path="ftp_path",
            )
            .with_columns(url=pl.col("ftp_path") + "/" + basename + "_genomic.gbff.gz")
        )

    def download(self, accession: str, progressbar=True):
        """ Download a reference genome from NCBI.

        Parameters
        ----------
        accession : str
            Assembly accession.
        progressbar : bool, default=True
            Show progress bar.
        """
        row = self.download_paths.filter(
            pl.col("accession") == accession
        ).to_dicts()[0]
        url = row["url"]
        return pooch.retrieve(
            url=url,
            known_hash=None,
            progressbar=progressbar,
            path=self.folder,
            fname=row["accession"] + ".gb.gz",
            processor=pooch.Decompress(name=row["accession"] + ".gb"),
        )

    def download_many(self, accessions, max_workers=None):
        """ Download multiple reference genomes from NCBI.

        Parameters
        ----------
        accessions : list
            List of assembly accessions.
        max_workers : int, default=None
            Number of threads to use.
        """
        return thread_map(
            partial(self.download, progressbar=False),
            accessions,
            max_workers=max_workers,
        )
