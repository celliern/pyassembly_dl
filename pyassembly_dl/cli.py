from enum import Enum
from pathlib import Path
from typing import List
from .core import AssemblyDownloader
import typer

app = typer.Typer()

class DBs(Enum):
    refseq = "refseq"
    genbank = "genbank"

@app.command()
def main(
    accession: List[str],
    subdir: str = "bacteria",
    db: DBs = "refseq",
    folder: Path = Path("./assembly"),
):
    folder.mkdir(parents=True, exist_ok=True)
    downloader = AssemblyDownloader(folder, db.value, subdir)
    if len(accession) == 1:
        typer.echo(downloader.download(accession[0]))
        return
    files = downloader.download_many(accession)
    for file in files:
        typer.echo(file)


if __name__ == "__main__":
    typer.run(app)
